const buttonEl = document.querySelector('button');
const inputEl = document.querySelector('input');
const ulEl = document.querySelector('ul');

function addgoal(){
    const entervalue = inputEl.value;
    const listEl = document.createElement('li');
    listEl.textContent = entervalue;
    ulEl.appendChild(listEl);
    inputEl.value = '';
}

buttonEl.addEventListener('click',addgoal);