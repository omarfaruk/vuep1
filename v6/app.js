Vue.createApp({
    data() {
        return {
            goals:[],
            value:'',
        };
    },

    methods: {
        addgoal()
        {
            this.goals.push(this.value);
            this.value = '';
        }
    }
}).mount('#app');